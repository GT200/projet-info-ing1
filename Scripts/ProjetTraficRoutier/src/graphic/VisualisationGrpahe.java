/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Dimension;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author tiazara
 */
public class VisualisationGrpahe extends JPanel{
        private DefaultCategoryDataset dataset;
        private JFreeChart chart;
        private ChartPanel CP;
        public VisualisationGrpahe(){
            this.dataset = new DefaultCategoryDataset();
            chart = this.createChart(this.dataset, "Nombre de voiture arretées au feu rouge", "Nombre de voiture arretées");
            this.CP = new ChartPanel(chart);
            this.CP.setPreferredSize(new Dimension(380, 380));
            this.add(CP);
        }
        public JFreeChart createChart(CategoryDataset dataset, String title, String ylabel) {
        
            this.chart = ChartFactory.createBarChart(
            title,                      // Titre
            "",                          // Axe des catégorie
            ylabel,                     // mesure
            dataset,                    // données
            PlotOrientation.VERTICAL,   // orientation
            true,                      // légende
            false,                      // tooltips?
            false                      // URLs?
        );
            return (this.chart);
        }
   
        public void updateGraphe(double value, String serie, String categorie, String title, String ylabel){
            this.dataset.addValue(value, serie, categorie);
            this.chart = this.createChart(dataset, title, ylabel);
            this.CP.repaint();
            //System.out.println(this.dataset.getRowCount()); 
        }
    }
