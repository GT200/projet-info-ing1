package road;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import org.locationtech.jts.geom.Coordinate;

/**
 * 
 * @author Tiazara Gaëtan && Stevie 
 */

public class Route {
        private int id;
	private double largeur;    // en mètre 
	private Coordinate point1;
	private Coordinate point2;    // point d'intersection 
	private FeuTricolor feu;
        private List<Voiture> listVoiture;
        private String orientation;    // Orientaition de la route dans un cas simple (Horizontale "H", Vertical"V")
	private int largeurAdapte;
        /**
         * 
         * @param id
         * @param largeur
         * @param point1
         * @param point2 : doit être le point d'intersention 
         * @param dureeFeuVert
         * @param EtatInitialFeu 
         */

        public Route(int id, double largeur, Coordinate point1, Coordinate point2, FeuTricolor feu) {
                this.id = id;
		this.largeur = largeur; 
                this.largeurAdapte = this.adapteLargeur(20);
		this.point1 = point1;
		this.point2 = point2;
		this.feu = feu;
                this.listVoiture = new ArrayList<>();
                this.roadOrientation();
                
	}

	public Coordinate getPoint1() { return this.point1;}
	public Coordinate getPoint2() {return this.point2;}
        public FeuTricolor getFeu(){return (this.feu);}
        public int getId(){return (this.id);}
        public List<Voiture> getListVoiture(){return (this.listVoiture);}
        
        public void roadOrientation(){
            System.out.println(this.point2);
            if((int) this.getPoint1().y == (int) this.getPoint2().y) {
                this.orientation = "H";
                if ((int)this.getPoint2().x > (int) this.getPoint1().x){
                    this.feu.setPosition(new Coordinate((int)(this.point2.x - this.largeurAdapte * 0.6 - this.feu.getTailleFeu()), (int)this.getPoint2().y +  this.largeurAdapte*0.4 + this.feu.getTailleFeu()));
                }
                else{
                    this.feu.setPosition(new Coordinate((int)this.getPoint2().x + this.largeurAdapte*0.4 + this.feu.getTailleFeu(), (int)this.getPoint2().y - this.largeurAdapte*0.6 - this.feu.getTailleFeu()));
                }
            }
            else if ((int) this.getPoint1().x == (int) this.getPoint2().x) {
                this.orientation = "V";
                if ((int)this.getPoint2().y > (int) this.getPoint1().y){
                    this.feu.setPosition(new Coordinate((int)this.getPoint2().x - this.largeurAdapte*0.6 - this.feu.getTailleFeu(), (int)this.getPoint2().y - this.largeurAdapte*0.6 - this.feu.getTailleFeu()));
                }
                else{
                    this.feu.setPosition(new Coordinate((int)this.getPoint2().x + this.largeurAdapte*0.6, (int)this.getPoint2().y + this.largeurAdapte*0.6));
                }
            }
        }
        
        // adapate la largeur réelle de la route par un facteur donnée pour pouvoir l'afficher correctement
        public int adapteLargeur(int facteur){return ((int) (this.largeur * facteur));}
        
        // adapate la direction de la voiture si celle ci n'est pas cohérente avec l'orientation de la route
        public void addVoiture(Voiture v){
            this.listVoiture.add(v);
            if (this.orientation.equals("H")){
                if (v.getDirection().equals("N") || v.getDirection().equals("S")){
                    v.setDirection("E");
                }
   
            }
            else if (this.orientation.equals("V")){
                if (v.getDirection().equals("E") || v.getDirection().equals("O")){
                    v.setDirection("S");
                }
            }
        }
        
        public void initPositionVoiture(Voiture v){
            if (this.orientation.equals("H")){
                if (v.getDirection().equals("O")){
                    v.setPosition(new Coordinate (this.point1.x, this.point1.y - v.getImgSize().width));
                }
                if (v.getDirection().equals("E")){
                    v.setPosition(new Coordinate (this.point1.x - v.getImgSize().width, this.point1.y));
                }
                if (v.getDirection().equals("N")|| v.getDirection().equals("S")){
                    v.setPosition(new Coordinate (this.point1.x, this.point1.y));
                }
            }
            else if (this.orientation.equals("V")){
                if (v.getDirection().equals("N")){
                    v.setPosition(new Coordinate (this.point1.x , this.point1.y - v.getImgSize().height));
                }
                if (v.getDirection().equals("S")){
                    v.setPosition(new Coordinate (this.point1.x - v.getImgSize().width, this.point1.y - v.getImgSize().height));
                }
                if (v.getDirection().equals("E")|| v.getDirection().equals("O")){
                    v.setPosition(new Coordinate (this.point1.x , this.point1.y ));
                }
            }  
        }
        
        public void driveOnRoad(Voiture v){
            if (v.getDirection().equals("E") || v.getDirection().equals("O")){
                // vérification de distance avec les voiture qui  sont devant v
                for (Voiture vi: this.listVoiture){
                    if (v.getImgSize().width<v.getPosition().distance(vi.getPosition()) && v.getPosition().distance(vi.getPosition()) < 1.2 * v.getImgSize().width){
                        if (v.getDeltaPosition() != 0 && v.getDirection().equals("E")){
                            if (vi.getPosition().x > v.getPosition().x){
                                v.setPosition(new Coordinate (v.getPosition().x - v.getDeltaPosition(), v.getPosition().y));
                            }
                            
                        }
                        if (v.getDeltaPosition() != 0 && v.getDirection().equals("O")){
                            if (vi.getPosition().x < v.getPosition().x){
                                v.setPosition(new Coordinate (v.getPosition().x + v.getDeltaPosition(), v.getPosition().y));
                            }
                        }
                    }
                }
                if (this.feu.getPosition().x - (this.largeurAdapte*0.2) < v.getHeadPosition().x && v.getHeadPosition().x < this.feu.getPosition().x){
                    
                    if (this.feu.getEtat().equals("rouge")){
                        v.stop();
                    }
                    else if (this.feu.getEtat().equals("vert")){
                        v.demare();
                        v.drive(v.getDirection());
                    }   
                }
                else {
                    v.drive(v.getDirection());  
                }
            }
            else if (v.getDirection().equals("N") || v.getDirection().equals("S")){
                // vérification de distance avec les voiture qui  sont devant v
                for (Voiture vi: this.listVoiture){
                    if (v.getImgSize().width<v.getPosition().distance(vi.getPosition()) && v.getPosition().distance(vi.getPosition()) < 1.2 * v.getImgSize().width){
                        if (v.getDeltaPosition() != 0 && v.getDirection().equals("N")){
                            if (vi.getPosition().y < v.getPosition().y){
                                v.setPosition(new Coordinate (v.getPosition().x , v.getPosition().y + v.getDeltaPosition()));
                            }
                        }
                        if (v.getDeltaPosition() != 0 && v.getDirection().equals("S")){
                            if (vi.getPosition().y > v.getPosition().y){
                                v.setPosition(new Coordinate (v.getPosition().x , v.getPosition().y - v.getDeltaPosition()));
                            }}
                    }
                }
                if (this.feu.getPosition().y < v.getHeadPosition().y && v.getHeadPosition().y < this.feu.getPosition().y  + (this.largeurAdapte*0.2)){
                    if (this.feu.getEtat().equals("rouge")){
                        v.stop();

                    }
                    else if (this.feu.getEtat().equals("vert")){
                        v.demare();
                        v.drive(v.getDirection());
                    }   
                }
                else {     
                                v.drive(v.getDirection());
      
                }
                
            }         
        }
	
        // prépare la position des feu et de l'orientation du polygon selon l'orientation de la route
        public int[][] checkOrientation(){
            int[][] polygon = new int[2][4];
            if(this.orientation.equals("H")) {
                int[] xPoints = {(int) this.point1.x,(int)this.point2.x,(int) this.point2.x,(int) this.point1.x};
                int[] yPoints = {(int)(this.point1.y - this.largeurAdapte/2),(int)(this.point2.y - this.largeurAdapte/2),(int)(this.point2.y + this.largeurAdapte/2),(int)(this.point1.y + this.largeurAdapte/2)};
                polygon[0] = xPoints;
                polygon[1] = yPoints;        
            }
            else if(this.orientation.equals("V")) {
                int[] xPoints = {(int) (this.getPoint1().x- this.largeurAdapte/2),(int)(this.getPoint1().x + this.largeurAdapte/2),(int) (this.getPoint2().x + this.largeurAdapte/2),(int) (this.getPoint2().x - this.largeurAdapte/2)};
                int[] yPoints = {(int)this.getPoint1().y, (int) this.getPoint1().y ,(int) this.getPoint2().y ,(int)this.getPoint2().y };
                polygon[0] = xPoints;
                polygon[1] = yPoints;
            }
            return (polygon);
        }
	public void draw(Graphics g) {
                int [] xPoints = this.checkOrientation()[0];
                int [] yPoints = this.checkOrientation()[1];
                int nPoints = 4;
                g.setColor(new Color(116, 121, 125));
                g.fillPolygon(xPoints, yPoints, nPoints);
		g.setColor(Color.black);
		//g.drawLine((int)this.getPoint1().x, (int)this.getPoint1().y, (int)this.getPoint2().x, (int)this.getPoint2().y);
                this.feu.draw(g);
	} 
        
        public void drawCarOnRoad(Graphics g){
            if (this.getListVoiture().size() != 0){
                    for (Voiture v : this.getListVoiture()){
                        v.draw(g);
                    }
                }
        }
	
}
