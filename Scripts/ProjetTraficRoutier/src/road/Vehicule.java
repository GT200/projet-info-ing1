/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import javax.swing.JComponent;
import org.locationtech.jts.geom.Coordinate;

/**
 * 
 * @author Tiazara Gaëtan
 */
public abstract class Vehicule extends JComponent{
    private int id;   // identifiant du véhicule (immatriculation)
    private Coordinate position;
    private int vitesse;
    private int deltaPosition; //adaptation du vitesse réel à l'échelle (pixelle) et au vitesse de rafraichissement
    
    /**
     * 
     * @param id         : identifiant de l'ojet voiture
     * @param vitesse : vitesse de la voiture
     * @param position   : position initiale
     */
    public Vehicule(int id, int vitesse,  Coordinate position){
        this.id = id;
        this.position = position;
        this.vitesse = vitesse;  
        this.adapationVitesse();
    }
    //Déplacement absolu
    // Déplacement selon l'axe des x (horizontale)
    public void deplacementEst(){
        Coordinate newPosition = new Coordinate(this.position.getX() + this.deltaPosition, this.position.getY());
        this.setPosition(newPosition);
    }
    public void deplacementOuest(){
        Coordinate newPosition = new Coordinate(this.position.getX() - this.deltaPosition, this.position.getY());
        this.setPosition(newPosition);
    }
    // Déplacement selon l'axe des y (vertical)
    public void deplacementSud(){
        Coordinate newPosition = new Coordinate(this.position.getX(), this.position.getY() + this.deltaPosition);
        this.position.setCoordinate(newPosition);
    }
    public void deplacementNord(){
        Coordinate newPosition = new Coordinate(this.position.getX(), this.position.getY() - this.deltaPosition);
        this.position.setCoordinate(newPosition);
    }
    
    public void stop(){
        this.deltaPosition = 0;
    }
    //  ne fait que demarer la voiture sans le déplacer
    public void demare(){
        this.adapationVitesse();
    }
    
    // adapte la vitesse de la voiture à léchelle des pixels (l'idée de prendre un certaine pourcentage de la vraie vitesse etait envisagée mais le réseltat était bof!
    public void adapationVitesse(){
        if (0<this.vitesse && this.vitesse<=20){
            this.deltaPosition = 1;
        }
        if (20<this.vitesse && this.vitesse<=40){
            this.deltaPosition = 2;
        }
        if (40<this.vitesse && this.vitesse<=60){
            this.deltaPosition = 3;
        }
        if (60<this.vitesse && this.vitesse<=80){
            this.deltaPosition = 4;
        }
        if (80<this.vitesse && this.vitesse<=100){
            this.deltaPosition = 5;
        }
        if (100<this.vitesse && this.vitesse<=120){
            this.deltaPosition = 6;
        }
        // ...
    }

    public int getId(){ return (this.id);}
    public Coordinate getPosition(){ return (this.position);}
    public void setPosition(Coordinate position){this.position = position;}
    public void setVitesse(int vitesse){this.vitesse = vitesse;}
    public int getDeltaPosition(){return this.deltaPosition;}

}
