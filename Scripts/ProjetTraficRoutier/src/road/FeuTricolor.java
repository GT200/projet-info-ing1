/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import java.awt.Color;
import java.awt.Graphics;
import org.locationtech.jts.geom.Coordinate;

public class FeuTricolor { 
    private int id;
    private Coordinate position;
    private String etat;    //état possile: vert, rouge 
    private int greenTime;
    private int tailleFeu;
    private Color couleur;
    
    public FeuTricolor(int id,int greenTime, String etat){
        this.id = id;
        this.greenTime = greenTime;
        this.etat      = etat;
        this.tailleFeu = 20;
    }
    
    public FeuTricolor(int greenTime, String etat){
        this.greenTime = greenTime;
        this.etat      = etat;
        this.tailleFeu = 20;
    }
    
    public FeuTricolor(int id, Coordinate position, int greenTime, String etat){
        this.id = id;
        this.position = position;
        this.greenTime = greenTime;
        this.etat      = etat;
        this.tailleFeu = 20;
    }
    
    public void checkColor(){
        if (this.etat.equals("vert")){
            this.couleur = Color.green;
        }
        else if (this.etat.equals("rouge")){
            this.couleur = Color.red;
        }
    }
    
    public void changeEtat(String etatDuFeu){
        if (etatDuFeu == "vert"){ this.etat = "rouge"; }
        else if (etatDuFeu == "rouge"){ this.etat = "vert"; }
    }
    
    public void draw(Graphics g){
        checkColor();
        g.setColor(this.couleur);
        g.fillOval((int) this.position.x, (int) this.position.y, this.tailleFeu, this.tailleFeu);  
        g.setColor(Color.black);
        g.drawOval((int) this.getPosition().x, (int) this.position.y, this.tailleFeu, this.tailleFeu);  
    }
    
    public void setEtat(String etat){this.etat = etat;}
    public String getEtat(){return (this.etat);}
    public int getTailleFeu(){return (this.tailleFeu);}
    public void setTailleFeu(int taille){this.tailleFeu = taille;}
    public Coordinate getPosition(){return (this.position);}
    public void setPosition(Coordinate position){this.position = position;}
}
