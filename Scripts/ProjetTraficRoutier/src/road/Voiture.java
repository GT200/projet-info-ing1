/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.locationtech.jts.geom.Coordinate;

/**
 * @author Tiazara Gaëtan
 * @since 2021-04-19
 */
public class Voiture extends Vehicule{
    
    private BufferedImage  imgVoitureHaut;
    private BufferedImage  imgVoitureDroite;
    private BufferedImage  imgVoitureBas;
    private BufferedImage  imgVoitureGauche;
    private BufferedImage  imgVoiture;
    private String direction;    
    private Dimension IMG_SIZE;
    private Coordinate headPosition;    // Position du devant de la voiture 
    
    /**
     * 
     * @param filenames  : une liste de nom de ficher d'une voiture orienter dans les 4 directions 
     * @param id         : identifiant de l'ojet voiture
     * @param vitesse : vitesse de la voiture en km/h
     * @param position   : position initiale
     * @param IMG_SIZE   : taille de la voiture 
     * @param direction  : direction de de la voiture (N, S , E, O)
     */
    public Voiture(String[] filenames, int id, int vitesse, Coordinate position, Dimension IMG_SIZE,String direction){
        super(id, vitesse, position);
        File fHaut = new File(filenames[0]);
        File fDroite = new File(filenames[1]);
        File fBas = new File(filenames[2]);
        File fGauche = new File(filenames[3]);
        this.direction = direction;
        try {
            this.imgVoitureHaut = ImageIO.read(fHaut);
            this.imgVoitureDroite = ImageIO.read(fDroite);
            this.imgVoitureBas = ImageIO.read(fBas);
            this.imgVoitureGauche = ImageIO.read(fGauche);
        } 
        catch (IOException e) {System.out.println("Problème de fichier");}
        this.IMG_SIZE = IMG_SIZE;  
        this.checkDirection();
    }
    
    public void drive(String direction){
        if (direction.equals("E")){
            super.deplacementEst();
        }     
        else if (direction.equals("O")){
            super.deplacementOuest();
        }
        else if (direction.equals("N")){
            super.deplacementNord();
        } 
        else if (direction.equals("S")){
            super.deplacementSud();
        }     
    }
    public void checkDirection(){
        if (this.direction == "N"){
            this.imgVoiture = this.imgVoitureHaut;
            this.headPosition = new Coordinate (super.getPosition().x - this.IMG_SIZE.width/2, super.getPosition().y);
        }
        else if (this.direction.equals("S")){
            this.imgVoiture = this.imgVoitureBas;
            this.headPosition = new Coordinate (super.getPosition().x - this.IMG_SIZE.width/2, super.getPosition().y + this.IMG_SIZE.height);
        }
        else if (this.direction.equals("E")){
            this.imgVoiture = this.imgVoitureDroite;
            this.headPosition = new Coordinate (super.getPosition().x + this.IMG_SIZE.width, super.getPosition().y + this.IMG_SIZE.width/2 );
        }
        else if (this.direction.equals("O")){
            this.imgVoiture = this.imgVoitureGauche;
            this.headPosition = new Coordinate (super.getPosition().x , super.getPosition().y + this.IMG_SIZE.width/2 );
        }
        //System.out.println("Cheecked direction");
    }
    
    // Dessine la voiture sans l'afficher 
    public void draw(Graphics g) {
        this.checkDirection();
        g.drawImage(this.imgVoiture, (int)super.getPosition().getX(), (int)super.getPosition().getY(), (int)this.getImgSize().width, (int)this.getImgSize().width, this);
    }
    
    public BufferedImage getImgVoiture(){return (this.imgVoiture);}
    public Dimension getImgSize(){return (this.IMG_SIZE);}
    public void setImgSize(Dimension size){this.IMG_SIZE = size ;}
    public void setDirection(String direction){this.direction = direction;}
    public String getDirection(){return (this.direction);}
    public Coordinate getHeadPosition(){return (this.headPosition);}
    
}
